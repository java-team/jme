This jar file contains the jMonkeyEngine XML/Java binding to support Collada
file format.

This file will rarely be modified, and will probably be distributed as-is, so
the classes were compiled without debug info (this applies only to the binary
variant of this file), and the jars are compressed with the compression level
set to 9 (with Ant).
